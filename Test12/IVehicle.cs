﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test12
{
    interface IVehicle
    {
        bool IsWorking();
        bool HasFuel();
        void PutFuel();
        void StartEngine();
        void PrintStateToSTDOUT();
    }
}
