﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test12
{
    class Car : IVehicle
    {
        private bool fuel = false;
        private bool working = false;
        public bool HasFuel() { if (fuel) return true; return false; }
        public bool IsWorking() { if (working) return true; return false; }
        public void PutFuel() { fuel = true; }
        public void StartEngine() { if (fuel) working = true; else return; }
        public void PrintStateToSTDOUT() { if (working) Console.WriteLine("Working....");
            else Console.WriteLine("NOT WORKING, NEEDS FUEL!");
        }
    }
}
