﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Test12
{
    class Program
    {
        static void Main(string[] args)
        {
            Car auto = new Test12.Car();
            Console.WriteLine("Trying to start.......\n");
            auto.PrintStateToSTDOUT();
            Console.WriteLine("Putting fuel........\n");
            
            for (int i = 1; i < 1000; i++,Thread.Sleep(4))
            {
                if ((i % 100) == 0)
                {
                    Console.SetCursorPosition(0, 6);
                    Console.Write("{0}%", i / 10);
                }
            }
            Console.WriteLine();
            auto.PutFuel();
            auto.StartEngine();
            auto.PrintStateToSTDOUT();
        }
       
    }
}
